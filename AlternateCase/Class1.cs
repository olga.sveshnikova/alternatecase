﻿using System;
using System.IO;

namespace AlternateCase
{
    public class AlternateCaseWriter: StreamWriter
    {
        public AlternateCaseWriter(Stream stream) : base(stream) { }
        public override void Write(string value)
        {
            char[] valueArray = value.ToCharArray();
           for (int i=0; i<valueArray.Length; i++) 
            {
                valueArray[i] = Char.IsLower(valueArray[i]) ? Char.ToUpper(valueArray[i]):Char.ToLower(valueArray[i]);
            }
            

            base.Write(valueArray);
        }
    }
}
